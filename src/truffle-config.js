module.exports = {

  networks: {
    development: {
		host: "127.0.0.1",
		port: 8545,
		network_id: "*",
		gas: 8000000,
		gasPrice: 65000000000
    }
  },

  mocha: {
    enableTimeouts: false
  },

  compilers: {
    solc: {
        optimizer: {
          enabled: false,
          runs: 200
        }
    }
  }
}

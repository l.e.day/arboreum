
const FakeDAI = require('../../build/contracts/FakeDAI')
const TrustNetwork = require('../../build/contracts/TrustNetwork')
const BondIssuance = require('../../build/contracts/BondIssuance')

module.exports = {

	async createAllContracts (web3, accounts, ownerAddress) {

		//console.log('Deploying Fake DAI Contracts...\n')
		let fakeDaiContract = new web3.eth.Contract(FakeDAI.abi)
		fdai = await fakeDaiContract.deploy({data: FakeDAI.bytecode, arguments: []}).send({from: ownerAddress, gas: 8000000})
		//console.log('... @ ' + fdai.options.address + '\n')

		//console.log('Deploying Bond Issuance Contract...')
		let bondIssuance = new web3.eth.Contract(BondIssuance.abi)
		bond = await bondIssuance.deploy({data: BondIssuance.bytecode,arguments: [fdai.options.address]}).send({from: ownerAddress, gas: 8000000})
		//console.log('... @ ' + bond.options.address + '\n')

		//console.log('Deploying Trust Network...')
		let trustNetwork = new web3.eth.Contract(TrustNetwork.abi)
		trust = await trustNetwork.deploy({data: TrustNetwork.bytecode, arguments: [bond.options.address, fdai.options.address]}).send({from: ownerAddress, gas: 8000000})
		//console.log('... @ ' + trust.options.address + '\n')
		
		return {
			fdai,
			trust,
			bond
		}
		
	}

}

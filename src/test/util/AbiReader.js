const Web3 = require('web3')
const web3 = new Web3()

module.exports = {
  getFunction (abi, name) {
    for (let i = 0; i < abi.length; i++) {
      if (abi[i].name === name) {
        return abi[i]
      }
    }
  },
  findAndDecodeEvent (abi, eventName, events) {
    abi = this.getFunction(abi, eventName)
    for (let key in events) {
      let topic0 = events[key].raw.topics[0]
      if (topic0 == abi.signature) {
        return web3.eth.abi.decodeLog(abi.inputs, events[key].raw.data, events[key].raw.topics)
      }
    }
    return null
  }
}
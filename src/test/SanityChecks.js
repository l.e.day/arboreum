const Web3      = require('web3')
const web3 		= new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8545'), null, { transactionConfirmationBlocks: 1 })
const BigNumber = require('bignumber')
const chai      = require('chai')
const assert 	= chai.assert
const should 	= chai.should()

const ArboreumDeployer = require('./util/ArboreumDeployer')
chai.use(require('./util/chai-bignumber')(BigNumber))

// Testing out whether or not the contracts actually connect to each other

describe('Base Logic: Sanity Checks\n', function () {
  
  let fakeDAI
  let trustNetwork

  let accounts

  // Before each test, create a new Arboreum framework
  beforeEach(async () => {
    accounts 		= await web3.eth.getAccounts()
	let arboreum 	= await ArboreumDeployer.createAllContracts(web3, accounts, accounts[0])

	fakeDAI 		= arboreum.fdai
	trustNetwork 	= arboreum.trust
	
	await fakeDAI.methods.approve(accounts[1], 100).send({from: accounts[0]})
	await fakeDAI.methods.approve(accounts[2], 100).send({from: accounts[0]})
	await fakeDAI.methods.approve(accounts[3], 100).send({from: accounts[0]})
	await fakeDAI.methods.transfer(accounts[1], 100).send({from: accounts[0]})
	await fakeDAI.methods.transfer(accounts[2], 100).send({from: accounts[0]})
	await fakeDAI.methods.transfer(accounts[3], 100).send({from: accounts[0]})
	let accDai = await fakeDAI.methods.balanceOf(accounts[1]).call({from: accounts[0]})
	assert.equal(100, accDai, '100 Fake DAI weren\'t transferred to account #1, instead got ' + accDai)
	await fakeDAI.methods.approve(trustNetwork.options.address, 100).send({from: accounts[1]})
	await fakeDAI.methods.approve(trustNetwork.options.address, 100).send({from: accounts[2]})
	await fakeDAI.methods.approve(trustNetwork.options.address, 100).send({from: accounts[3]})
  })
  
  it('should allow a user to deposit DAI to the Trust contract as equity', async function () {
	await trustNetwork.methods.depositEquity(100).send({from: accounts[3], gas: 8000000})
	let acc3Equity = await trustNetwork.methods.personalEquityBalance.call({from: accounts[3]})
	assert.equal(100, acc3Equity, '100 DAI worth of equity was not deposited for account #3')
  })
    
  it('should allow an equity holder to transfer up to half of their equity into trust', async function () {
	await trustNetwork.methods.depositEquity(100).send({from: accounts[3], gas: 8000000})
	await trustNetwork.methods.convertEquityToTrust(50).send({from: accounts[3]})
	let acc3Trust  = await trustNetwork.methods.personalTrustBalance.call({from: accounts[3]})
	assert.equal(50, acc3Trust, '50 DAI worth of equity was not converted into trust for account #3')
  })
     
  it('should forbid an equity holder from transferring more than half of their equity into trust', async function () {
	await trustNetwork.methods.depositEquity(100).send({from: accounts[3], gas: 8000000})
	await trustNetwork.methods.convertEquityToTrust(60).send({from: accounts[3]}).then(function () {
          assert(false, 'Expected Solidity to throw here.')
        }).catch((err) => {
          if (err.toString().indexOf('revert') != -1) {
          } else {
            assert(false, err.toString())
          }
        })	
  })

  it('should allow a user to round-trip DAI -> equity -> trust and back, from deposit to withdrawal', async function () {
	await trustNetwork.methods.depositEquity(100).send({from: accounts[3], gas: 8000000})
	await trustNetwork.methods.convertEquityToTrust(50).send({from: accounts[3]})
	await trustNetwork.methods.convertTrustToEquity(50).send({from: accounts[3]})
	await trustNetwork.methods.withdrawEquity(100).send({from: accounts[3]})
	let acc3Equity = await trustNetwork.methods.personalEquityBalance.call({from: accounts[3]})
	let acc3Trust  = await trustNetwork.methods.personalTrustBalance.call({from: accounts[3]})
	let acc3Dai	   = await fakeDAI.methods.balanceOf(accounts[3]).call({from: accounts[3]})
	assert.equal(0, acc3Equity, "User has withdrawn all equity, but is still logged as holding some.")
	assert.equal(0, acc3Trust,  "User has exchanged all trust, but is still logged as holding some.")
	assert.equal(100, acc3Dai,  "User has withdrawn all equity, but the DAI has not been returned.")
  })
  
  it('should allow a trust holder to create a new trust network', async function () {
	await trustNetwork.methods.depositEquity(100).send({from: accounts[3], gas: 8000000})
	await trustNetwork.methods.convertEquityToTrust(50).send({from: accounts[3]})
    await trustNetwork.methods.createTrustNetwork().send({from: accounts[3], gas: 8000000})
	let exiNetworks  = await trustNetwork.methods.networksExisting.call({from: accounts[3]})
	assert.equal(1, exiNetworks, 'A new trust network wasn\'t created as expected.')
  })
  
  it('should forbid trust network members from joining a network they are already in', async function () {
	await trustNetwork.methods.depositEquity(100).send({from: accounts[3], gas: 8000000})
	await trustNetwork.methods.convertEquityToTrust(50).send({from: accounts[3]})
    await trustNetwork.methods.createTrustNetwork().send({from: accounts[3], gas: 8000000})
    await trustNetwork.methods.joinTrustNetwork(0).send({from: accounts[3], gas: 8000000}).then(function () {
          assert(false, 'Expected Solidity to throw here.')
        }).catch((err) => {
          if (err.toString().indexOf('revert') != -1) {
          } else {
            assert(false, err.toString())
          }
        })	
  })

  it('should forbid users from adding other users to trust networks they are not themselves in', async function () {
	await trustNetwork.methods.depositEquity(100).send({from: accounts[3], gas: 8000000})
	await trustNetwork.methods.depositEquity(100).send({from: accounts[2], gas: 8000000})
	await trustNetwork.methods.convertEquityToTrust(50).send({from: accounts[3]})
	await trustNetwork.methods.convertEquityToTrust(50).send({from: accounts[2]})
    await trustNetwork.methods.createTrustNetwork().send({from: accounts[3], gas: 8000000})
    await trustNetwork.methods.createTrustNetwork().send({from: accounts[2], gas: 8000000})
    await trustNetwork.methods.addMemberToTrustNetwork(accounts[2], 0).send({from: accounts[3], gas: 8000000}).then(function () {
          assert(false, 'Expected Solidity to throw here.')
        }).catch((err) => {
          if (err.toString().indexOf('revert') != -1) {
          } else {
            assert(false, err.toString())
          }
        })	
  })
   
  it('should allow a network member to deposit trust into said network', async function () {
	await trustNetwork.methods.depositEquity(100).send({from: accounts[3], gas: 8000000})
	await trustNetwork.methods.convertEquityToTrust(50).send({from: accounts[3]})
    await trustNetwork.methods.createTrustNetwork().send({from: accounts[3], gas: 8000000})
    await trustNetwork.methods.depositTrustToNetwork(0, 50).send({from: accounts[3], gas: 8000000})
	let trustNetWorth  = await trustNetwork.methods.networkTrustBalance(0).call({from: accounts[3]})
	assert.equal(50, trustNetWorth, 'Expected 50 DAI in trust in network #0, got ' + trustNetWorth)
  })
  
  it('should allow a network member to extend trust to another network member', async function () {
	await trustNetwork.methods.depositEquity(100).send({from: accounts[3], gas: 8000000})
	await trustNetwork.methods.convertEquityToTrust(50).send({from: accounts[3]})
    await trustNetwork.methods.createTrustNetwork().send({from: accounts[3], gas: 8000000})
    await trustNetwork.methods.depositTrustToNetwork(0, 50).send({from: accounts[3], gas: 8000000})
    await trustNetwork.methods.joinTrustNetwork(0).send({from: accounts[2], gas: 8000000})
    await trustNetwork.methods.transferTrustWithinNetwork(0, accounts[2], 40).send({from: accounts[3], gas: 8000000})
	let trustNetWorth3  = await trustNetwork.methods.networkTrustBalance(0).call({from: accounts[3]})
	assert.equal(50, trustNetWorth3, 'Expected 50 DAI in trust in network #0 for account #3, got ' + trustNetWorth3)
	let trustNetWorth2  = await trustNetwork.methods.networkTrustBalance(0).call({from: accounts[2]})
	assert.equal(40, trustNetWorth2, 'Expected 40 DAI in trust in network #0 for account #2, got ' + trustNetWorth2)
  })
  
  it('should allow a network member granted trust to withdraw it all as DAI', async function () {
	await trustNetwork.methods.depositEquity(100).send({from: accounts[3], gas: 8000000})
	await trustNetwork.methods.convertEquityToTrust(50).send({from: accounts[3]})
    await trustNetwork.methods.createTrustNetwork().send({from: accounts[3], gas: 8000000})
    await trustNetwork.methods.depositTrustToNetwork(0, 50).send({from: accounts[3], gas: 8000000})
    await trustNetwork.methods.joinTrustNetwork(0).send({from: accounts[2], gas: 8000000})
    await trustNetwork.methods.transferTrustWithinNetwork(0, accounts[2], 40).send({from: accounts[3], gas: 8000000})
    await trustNetwork.methods.withdrawTrustFromNetwork(0, 40).send({from: accounts[2], gas: 8000000})
	await trustNetwork.methods.convertTrustToEquity(40).send({from: accounts[2], gas: 8000000})
	await trustNetwork.methods.withdrawEquity(40).send({from: accounts[2], gas: 8000000})
    
  })
  
  it('should forbid a network member granted trust to withdraw more than it is allowed', async function () {
	await trustNetwork.methods.depositEquity(100).send({from: accounts[3], gas: 8000000})
	await trustNetwork.methods.convertEquityToTrust(50).send({from: accounts[3]})
    await trustNetwork.methods.createTrustNetwork().send({from: accounts[3], gas: 8000000})
    await trustNetwork.methods.depositTrustToNetwork(0, 50).send({from: accounts[3], gas: 8000000})
    await trustNetwork.methods.joinTrustNetwork(0).send({from: accounts[2], gas: 8000000})
    await trustNetwork.methods.transferTrustWithinNetwork(0, accounts[2], 40).send({from: accounts[3], gas: 8000000})
    await trustNetwork.methods.withdrawTrustFromNetwork(0, 50).send({from: accounts[2], gas: 8000000}).then(function () {
          assert(false, 'Expected Solidity to throw here.')
        }).catch((err) => {
          if (err.toString().indexOf('revert') != -1) {
          } else {
            assert(false, err.toString())
          }
        })
  })
  
  it('should allow a network member to modify the amount of trust they extend to another member', async function () {
	await trustNetwork.methods.depositEquity(100).send({from: accounts[3], gas: 8000000})
	await trustNetwork.methods.convertEquityToTrust(50).send({from: accounts[3]})
    await trustNetwork.methods.createTrustNetwork().send({from: accounts[3], gas: 8000000})
    await trustNetwork.methods.depositTrustToNetwork(0, 50).send({from: accounts[3], gas: 8000000})
    await trustNetwork.methods.joinTrustNetwork(0).send({from: accounts[2], gas: 8000000})
    await trustNetwork.methods.transferTrustWithinNetwork(0, accounts[2], 40).send({from: accounts[3], gas: 8000000})
    await trustNetwork.methods.modifyTrustLevel(0, accounts[2], 50).send({from: accounts[3], gas: 8000000})    
  })
    
  it('should forbid a network member from modifying the amount of trust they extend to more than they have', async function () {
	await trustNetwork.methods.depositEquity(100).send({from: accounts[3], gas: 8000000})
	await trustNetwork.methods.convertEquityToTrust(50).send({from: accounts[3]})
    await trustNetwork.methods.createTrustNetwork().send({from: accounts[3], gas: 8000000})
    await trustNetwork.methods.depositTrustToNetwork(0, 50).send({from: accounts[3], gas: 8000000})
    await trustNetwork.methods.joinTrustNetwork(0).send({from: accounts[2], gas: 8000000})
    await trustNetwork.methods.transferTrustWithinNetwork(0, accounts[2], 40).send({from: accounts[3], gas: 8000000})
    await trustNetwork.methods.modifyTrustLevel(0, accounts[2], 60).send({from: accounts[3], gas: 8000000}).then(function () {
          assert(false, 'Expected Solidity to throw here.')
        }).catch((err) => {
          if (err.toString().indexOf('revert') != -1) {
          } else {
            assert(false, err.toString())
          }
        })
  })
    
  it('should forbid a network member from modifying the amount of trust they extend to the same value', async function () {
	await trustNetwork.methods.depositEquity(100).send({from: accounts[3], gas: 8000000})
	await trustNetwork.methods.convertEquityToTrust(50).send({from: accounts[3]})
    await trustNetwork.methods.createTrustNetwork().send({from: accounts[3], gas: 8000000})
    await trustNetwork.methods.depositTrustToNetwork(0, 50).send({from: accounts[3], gas: 8000000})
    await trustNetwork.methods.joinTrustNetwork(0).send({from: accounts[2], gas: 8000000})
    await trustNetwork.methods.transferTrustWithinNetwork(0, accounts[2], 40).send({from: accounts[3], gas: 8000000})
    await trustNetwork.methods.modifyTrustLevel(0, accounts[2], 40).send({from: accounts[3], gas: 8000000}).then(function () {
          assert(false, 'Expected Solidity to throw here.')
        }).catch((err) => {
          if (err.toString().indexOf('revert') != -1) {
          } else {
            assert(false, err.toString())
          }
        })
  })
  
})
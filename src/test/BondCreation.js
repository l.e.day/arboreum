const Web3      = require('web3')
const web3 		= new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8545'), null, { transactionConfirmationBlocks: 1 })
const BigNumber = require('bignumber')
const chai      = require('chai')
const assert 	= chai.assert
const should 	= chai.should()

const ArboreumDeployer = require('./util/ArboreumDeployer')
chai.use(require('./util/chai-bignumber')(BigNumber))

// Testing out whether or not the contracts actually connect to each other

describe('Bond Logic: Proposing and Funding\n', function () {
  
  let fakeDAI
  let trustNetwork

  let accounts

  // Before each test, create a new Arboreum framework
  beforeEach(async () => {
		accounts 		= await web3.eth.getAccounts()
		let arboreum 	= await ArboreumDeployer.createAllContracts(web3, accounts, accounts[0])

		fakeDAI 		= arboreum.fdai
		trustNetwork 	= arboreum.trust
		
		await fakeDAI.methods.approve(accounts[1], 10000).send({from: accounts[0]})
		await fakeDAI.methods.approve(accounts[2], 10000).send({from: accounts[0]})
		await fakeDAI.methods.approve(accounts[3], 10000).send({from: accounts[0]})
		await fakeDAI.methods.transfer(accounts[1], 10000).send({from: accounts[0]})
		await fakeDAI.methods.transfer(accounts[2], 10000).send({from: accounts[0]})
		await fakeDAI.methods.transfer(accounts[3], 10000).send({from: accounts[0]})
		let accDai = await fakeDAI.methods.balanceOf(accounts[1]).call({from: accounts[0]})
		assert.equal(10000, accDai, '10000 Fake DAI weren\'t transferred to account #1, instead got ' + accDai)
		await fakeDAI.methods.approve(trustNetwork.options.address, 10000).send({from: accounts[1]})
		await fakeDAI.methods.approve(trustNetwork.options.address, 10000).send({from: accounts[2]})
		await fakeDAI.methods.approve(trustNetwork.options.address, 10000).send({from: accounts[3]})
	})
  
    it('should allow a network member to propose a bond if it meets the requisite conditions', async function () {
		await trustNetwork.methods.depositEquity(10000).send({from: accounts[1], gas: 8000000})
		await trustNetwork.methods.depositEquity(1000).send({from: accounts[2], gas: 8000000})
		await trustNetwork.methods.convertEquityToTrust(5000).send({from: accounts[1]})
		await trustNetwork.methods.createTrustNetwork().send({from: accounts[1], gas: 8000000})
		await trustNetwork.methods.depositTrustToNetwork(0, 5000).send({from: accounts[1], gas: 8000000})
		await trustNetwork.methods.joinTrustNetwork(0).send({from: accounts[2], gas: 8000000})
		await trustNetwork.methods.transferTrustWithinNetwork(0, accounts[2], 5000).send({from: accounts[1], gas: 8000000})
		await trustNetwork.methods.depositEquity(10000).send({from: accounts[3], gas: 8000000})
		await trustNetwork.methods.registerEmail("0xstarscream@protonmail.com").send({from: accounts[2], gas: 8000000})
		let sixMonths = 24 * 60800
		await trustNetwork.methods.proposeBond(0, accounts[2], accounts[1], 1000, 4000, 10000, 20, sixMonths).send({from: accounts[2], gas: 8000000})
    })
  
    it('should prevent a bond from being proposed if there isn\'t sufficient direct principal protection', async function () {
		await trustNetwork.methods.depositEquity(10000).send({from: accounts[1], gas: 8000000})
		await trustNetwork.methods.depositEquity(1000).send({from: accounts[2], gas: 8000000})
		await trustNetwork.methods.convertEquityToTrust(5000).send({from: accounts[1]})
		await trustNetwork.methods.createTrustNetwork().send({from: accounts[1], gas: 8000000})
		await trustNetwork.methods.depositTrustToNetwork(0, 5000).send({from: accounts[1], gas: 8000000})
		await trustNetwork.methods.joinTrustNetwork(0).send({from: accounts[2], gas: 8000000})
		await trustNetwork.methods.transferTrustWithinNetwork(0, accounts[2], 5000).send({from: accounts[1], gas: 8000000})
		await trustNetwork.methods.depositEquity(10000).send({from: accounts[3], gas: 8000000})
		await trustNetwork.methods.registerEmail("0xstarscream@protonmail.com").send({from: accounts[2], gas: 8000000})
		let sixMonths = 24 * 60800
		await trustNetwork.methods.proposeBond(0, accounts[2], accounts[1], 1000, 4000, 20000, 20, sixMonths).send({from: accounts[2], gas: 8000000}).then(function () {
			assert(false, 'Expected Solidity to throw here.')
		  }).catch((err) => {
			if (err.toString().indexOf('revert') != -1) {
			} else {
			  assert(false, err.toString())
			}
		  })
		})
  })
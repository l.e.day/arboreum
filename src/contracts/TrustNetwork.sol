pragma solidity ^0.5.8;

import "./token/interfaces/Bond_Interface.sol";
import "./token/interfaces/ERC20_Interface.sol";
import "./utils/SafeMath.sol";
import "./utils/Reentrancy_Guard.sol";

contract TrustNetwork is Reentrancy_Guard {
    using SafeMath for uint;
    ERC20_Interface public daiToken;
    Bond_Interface public bondContract;

    /**
     *  Constants and Variables
     **/

    uint internal constant                    MAX_INT = 2**256 - 1;
    uint internal constant                    magnitude = 10**18;

    uint private                              totalEquityHeld;
    uint private                              totalTrustHeld;

    mapping (address => uint)                 personalEquity;
    mapping (address => uint)                 personalTrust;

    mapping(uint => address[])                networkMembers;
    mapping(uint =>
        mapping(address => uint))             networkTrustAllowance;
    mapping(uint => uint)                     networkTrustTotal;
    mapping(address => uint[])                networkMemberships;
    mapping(uint =>
        mapping(address =>
            mapping(address => uint)))        networkTransferAmounts;

	mapping(address => string) internal		  registeredEmails;
	mapping(address => bool) internal		  hasRegisteredEmail;

    uint internal                             currentHighestNetworkIndex;

    address internal                 		  administrator;

    address private _bondAddress;
    address private _daiAddress;
    Bond_Interface  _bondInterface;
    ERC20_Interface _daiInterface;

    constructor (address _bond, address _dai) public {
		_bondAddress = _bond;
		_bondInterface = Bond_Interface(_bondAddress);
        bondContract = _bondInterface;
        _daiAddress = _dai;
		_daiInterface = ERC20_Interface(_daiAddress);
		daiToken = _daiInterface;
		daiToken.approve(_bondAddress, MAX_INT);
        currentHighestNetworkIndex = 0;
        administrator = msg.sender;
    }

    /**
     *  Modifiers
     **/

    modifier isEquityHolder(){
        require(personalEquity[msg.sender] > 0, "Modifier 'isEquityHolder' requires the caller to have a non-zero equity balance.");
        _;
    }

    modifier isTrustHolder(){
        require(personalTrust[msg.sender] > 0, "Modifier 'isTrustHolder' requires the caller to have a non-zero trust balance.");
        _;
    }

    modifier isAdministrator(){
        _isAdministrator();
        _;
    }

    enum Participant {
        Beneficiary,
        Guarantor,
        Investor
    }

    /**
     *  Events
     **/

    event EquityDeposited(address depositor, uint amount, uint timestamp);
    event EquityWithdrawn(address withdrawer, uint amount, uint timestamp);
    event TrustTokensMinted(address minter, uint amountDAI, uint timestamp);
    event TrustTokensBurned(address burner, uint amountTrust,uint    timestamp);
    event TrustNetworkCreated(address creator, uint networkIndex, uint    timestamp);
    event TrustDeposited(address depositor, uint networkIndex, uint amount, uint timestamp);
    event TrustWithdrawn(address withdrawer, uint networkIndex, uint amount, uint timestamp);
    event TrustTransfer(address sender, address receiver, uint networkIndex, uint trustSent, uint timestamp);
    event TrustModification(address sender, address recipient, uint networkIndex, uint existingTrust, uint newTrust, uint timestamp);
    event TrustNetworkMemberAdded(address adder, address addee, uint networkIndex, uint timestamp);
    event TrustNetworkMemberJoined(address joiner, uint networkIndex, uint timestamp);
    event ZerothCollateralTransferred(uint bondIndex, address payer, uint amount, uint timestamp);
    event FirstCollateralTransferred(uint bondIndex, address payer, uint amount, uint timestamp);	
	event DaiContractSwitched(address newDAI, uint timestamp);

    /**
     *  Core Functionality
     **/

     function setNewBondContract(address _newBondAddress) public isAdministrator {
         daiToken.approve(_bondAddress, 0);
         _bondAddress = _newBondAddress;
		 daiToken.approve(_bondAddress, MAX_INT);
     }

     // For our purposes, this to only be used for plugging in fake DAI's for testing on Ropsten.
     // More generally, it can be used in the event of DAI reinstantiating to a different contract.
     function setNewDAIContract(address _newDAIAddress) public isAdministrator {
		 daiToken.approve(_bondAddress, 0);
         _daiAddress = _newDAIAddress;
		 _daiInterface = ERC20_Interface(_daiAddress);
		 daiToken = _daiInterface;
         daiToken.approve(_bondAddress, MAX_INT);
		 emit DaiContractSwitched(_newDAIAddress, now);
     }

	function viewCurrentDAIContract() public view returns (address) {
		return _daiAddress;
	}

	function viewCurrentBondContract() public view returns (address) {
		return _bondAddress;
	}
	
	function registerEmail(string memory _email) public returns (bool success) {
		registeredEmails[msg.sender] = _email;
		hasRegisteredEmail[msg.sender] = true;
		return true;
	}

    /**
     *  Note that as it stands, the guarantor will have to withdraw the amount of trust they agree to be first-loss collateral,
     *   and convert it back to personal equity in order to transfer it to the bond contract.
     */

    function transferBondCollateral(uint _bondIndex, uint _amount) public {
        require(daiToken.approve(_bondAddress, _amount), "transferBondCollateral: cannot approve enough DAI tokens");
        require(personalEquity[msg.sender] >= _amount, "transferBondCollateral: cannot transfer more equity than held");
        address actor = msg.sender;
        (address b, address g, uint bcol, bool bpay, uint gcol, bool gpay,) = bondContract.serialiseBondPaymentDetails(_bondIndex);
        if (address(actor) == b) {
            require(bcol == _amount, "transferBondCollateral: insufficient amount to cover beneficiary collateral.");
            require(!bpay, "transferBondCollateral: beneficiary collateral already paid.");
            require(daiToken.transferFrom(actor, _bondAddress, _amount), "transferBondCollateral: transferFrom failed for beneficiary collateral.");
            personalEquity[actor] -= _amount;
            totalEquityHeld -= _amount;
            bondContract.acknowledgeCollateralDeposit(_bondIndex, actor);
            emit ZerothCollateralTransferred(_bondIndex, actor, _amount, now);
        }
        else {
            if (address(actor) == g) {
                require(gcol == _amount, "transferBondCollateral: insufficient amount to cover guarantor collateral.");
                require(!gpay, "transferBondCollateral: guarantor collateral already paid.");
                // The guarantor(s) is/are transferring DAI to the contract (first loss protection)
                require(daiToken.transferFrom(actor, _bondAddress, _amount), "transferBondCollateral: transferFrom failed for guarantor collateral.");
                personalEquity[actor] -= _amount;
                totalEquityHeld -= _amount;
                bondContract.acknowledgeCollateralDeposit(_bondIndex, actor);
                emit FirstCollateralTransferred(_bondIndex, actor, _amount, now);
            }
            else {revert("transferBondCollateral: attempting to fund a bond when neither beneficiary or guarantor.");}
        }
    }

    // Should the below be administrator only, or delegated somewhat?
    function transferEquityPoolCapital(uint _bondIndex, uint _amount) public isAdministrator {
        require(_amount <= totalEquityHeld, "transferEquityPoolCapital: not enough equity pooled to provide second loss protection.");
       (,,, bool bpay,, bool gpay, bool spay) = bondContract.serialiseBondPaymentDetails(_bondIndex);
       require(bpay && gpay && !spay, "transferEquityPoolCapital: both guarantor and beneficiary collateral must be provided first.");
       require(bondContract.equityFundBond(_bondIndex), "transferEquityPoolCapital: could not transfer remaining equity.");
    }

    function depositEquity(uint amount) public {
		uint existingApproval = daiToken.allowance(address(this), msg.sender);
		uint updatedApproval = existingApproval + amount;
		daiToken.approve(msg.sender, updatedApproval);
        daiToken.transferFrom(msg.sender, address(this), amount); // amount must have been separately pre-approved via an interaction with DAI
        totalEquityHeld += amount;
        personalEquity[msg.sender] += amount;
        emit EquityDeposited(msg.sender, amount, now);
    }

    function withdrawEquity(uint amount) public nonReentrant {
        require(amount <= personalEquity[msg.sender], "withdrawEquity: cannot withdraw more equity than is held.");
        totalEquityHeld -= amount;
        require(daiToken.transfer(msg.sender, amount), "withdrawEquity: transfer failed");
        personalEquity[msg.sender] -= amount;
        emit EquityWithdrawn(msg.sender, amount, now);
    }

    function convertEquityToTrust(uint amount) public isEquityHolder nonReentrant {
        uint equity = personalEquity[msg.sender];
        require(amount < equity, "convertEquityToTrust: cannot convert more equity than available to trust.");
        uint trustEquivalentDAI = personalTrust[msg.sender];
        uint resultingTrustDAI  = trustEquivalentDAI + amount;
        uint resultingEquityDAI = equity - amount;
        require (resultingTrustDAI <= resultingEquityDAI, "convertEquityToTrust: the ratio of equity to trust must be AT MOST 1:1");
        // Mint new tokens
        totalTrustHeld += amount;
        personalTrust[msg.sender] += amount;
        personalEquity[msg.sender] -= amount;
        emit TrustTokensMinted(msg.sender, amount, now);
    }

    function convertTrustToEquity(uint amount) public isTrustHolder nonReentrant {
        uint trust = personalTrust[msg.sender];
        require(amount <= trust, "convertTrustToEquity: cannot convert more trust than is held.");
        personalTrust[msg.sender] -= amount;
        personalEquity[msg.sender] += amount;
        totalTrustHeld -= amount;
        totalEquityHeld += amount;
        emit TrustTokensBurned(msg.sender, amount, now);
    }

    // Create a new trust network containing only the caller.
    function createTrustNetwork() public isEquityHolder isTrustHolder nonReentrant returns (uint) {
        uint latestGroup = currentHighestNetworkIndex;
        networkMembers[latestGroup].push(msg.sender);
        networkMemberships[msg.sender].push(latestGroup);
        networkTrustTotal[latestGroup] = 0;
        currentHighestNetworkIndex += 1;
        emit TrustNetworkCreated(msg.sender, latestGroup, now);
        return latestGroup;
    }

    function depositTrustToNetwork(uint networkIndex, uint amount) public nonReentrant {
        uint depositorsTrust = personalTrust[msg.sender];
        require (amount <= depositorsTrust, "depositTrustToNetwork: cannot deposit more trust than is owned.");
        networkTrustTotal[networkIndex] += amount;
        networkTrustAllowance[networkIndex][msg.sender] = amount;
        personalTrust[msg.sender] -= amount;
        emit TrustDeposited(msg.sender, networkIndex, amount, now);
    }

    function withdrawTrustFromNetwork(uint networkIndex, uint amount) public nonReentrant {
        require (amount <= networkTrustAllowance[networkIndex][msg.sender], "withdrawTrustFromNetwork: user withdrawing more trust than assigned.");
        require (amount <= networkTrustTotal[networkIndex], "withdrawTrustFromNetwork: withdrawing more trust than available in network.");
        networkTrustTotal[networkIndex] -= amount;
		networkTrustAllowance[networkIndex][msg.sender] -= amount;
        personalTrust[msg.sender] += amount;
        emit TrustWithdrawn(msg.sender, networkIndex, amount, now);
    }

    function addMemberToTrustNetwork(address joiner, uint networkIndex) public nonReentrant {
        require(_isNetworkMember(joiner, networkIndex), "addMemberToTrustNetwork: callee is already a member of that trust network.");
        networkMembers[networkIndex].push(joiner);
        networkMemberships[joiner].push(networkIndex);
        emit TrustNetworkMemberAdded(msg.sender, joiner, networkIndex, now);
    }

    function joinTrustNetwork(uint networkIndex) public {
        require(!_isNetworkMember(msg.sender, networkIndex), "joinTrustNetwork: cannot join a network you are already a part of");
        networkMembers[networkIndex].push(msg.sender);
        networkMemberships[msg.sender].push(networkIndex);
        emit TrustNetworkMemberJoined(msg.sender, networkIndex, now);
    }

    function transferTrustWithinNetwork(uint networkIndex, address recipient, uint amount) public nonReentrant {

        require(_isNetworkMember(msg.sender, networkIndex), "transferTrustWithinNetwork: msg.sender is not a member of this trust network.");
        require(networkTrustAllowance[networkIndex][msg.sender] >= amount, "transferTrustWithinNetwork: can't transfer this much trust.");
        require(amount <= networkTrustTotal[networkIndex], "transferTrustWithinNetwork: amount transferred must be LTE to the network total");

        /**
         *  If the recipient has already transferred trust to the function caller within this network,
         *  the function caller will have to create a new trust network and extend trust THERE. The risk
         *  of allowing it to be done here is the potential to mint infinite credit lines between two parties
         *  constantly transferring trust to each other, since no deductions are made from the senders balance.
         **/

        require(networkTransferAmounts[networkIndex][recipient][msg.sender] == 0,
                "transferTrustWithinNetwork: cannot transfer credit to an existing creditor within the same trust network. Use modifyTrustLevel.");

        /**
        *   Here we ensure that someone can't be allocated more trust than they have the right to
        *   grant, and that it doesn't exceed the total amount associated to the network. Note that
        *   we don't deduct the amount transferred from the sender, as this function constitutes a line
        *   of credit extension to @recipient.
        **/

        if ((networkTrustAllowance[networkIndex][recipient] + amount) >= networkTrustTotal[networkIndex]) {
            networkTrustAllowance[networkIndex][recipient] = networkTrustTotal[networkIndex];
        }
        else {
            networkTrustAllowance[networkIndex][recipient] += amount;
        }
        networkTransferAmounts[networkIndex][msg.sender][recipient] = amount;
        emit TrustTransfer(msg.sender, recipient, networkIndex, amount, now);
    }

    function modifyTrustLevel(uint networkIndex, address recipient, uint newAmount) public nonReentrant {
        require(_isNetworkMember(msg.sender, networkIndex), "modifyTrustLevel: caller must be a member of the given trust network.");
        uint existingTrust = networkTransferAmounts[networkIndex][msg.sender][recipient];
        require(networkTrustAllowance[networkIndex][msg.sender] >= newAmount, "modifyTrustLevel: cannot modify trust level to more than the level of trust held.");
        require(!(existingTrust == newAmount), "modifyTrustLevel: cannot modify trust level to the amount already extended.");
        uint higherValue = max(existingTrust, newAmount);
        if (higherValue == newAmount) {
            uint diff = newAmount - existingTrust;
            networkTrustAllowance[networkIndex][recipient] += diff;
        }
        else {
            uint diff = existingTrust - (existingTrust - newAmount);
            networkTrustAllowance[networkIndex][recipient] -= diff;
        }
        emit TrustModification(msg.sender, recipient, networkIndex, existingTrust, newAmount, now);
    }

    function proposeBond(uint           _networkIndex,
                         address        _beneficiary,
                         address        _guarantor,
                         uint           _selfCollateral,
                         uint           _firstLossCollateral,
                         uint           _principal,
                         uint           _yield,
                         uint           _expiry)
                         public {
		// Bond beneficiary must have an associated email address for communication.
		require(hasRegisteredEmail[_beneficiary], "proposeBond: beneficiary must have a registered email address");
        // Both the guarantor and the beneficiary need to be in the same trust network.
        require(_isNetworkMember(_beneficiary, _networkIndex) && _isNetworkMember(_guarantor, _networkIndex),
                "proposeBond: both beneficiary and guarantor must be in same network");
        uint _beneficiaryCreditLine = personalEquity[_beneficiary];
        uint _guarantorCreditLine = networkTransferAmounts[_networkIndex][_guarantor][_beneficiary];
        require (_selfCollateral <= _beneficiaryCreditLine, "proposeBond: beneficiary must have sufficient equity for proposal.");
		require (_guarantorCreditLine > 0, "proposeBond: beneficiary must have guarantor as a direct trust parent.");
        // The guarantor has to have explicitly granted trust to the beneficiary - for our MVP only one persons' trust will be taken as first-loss collateral.
        require(_firstLossCollateral <= _guarantorCreditLine, "proposeBond: guarantor must have extended sufficient credit for proposal.");
        bondContract.proposeBond(_networkIndex,_beneficiary, _guarantor, _selfCollateral,
                                 _firstLossCollateral, _principal, _yield, _expiry);
		// event is fired off by BondIssuance, but we should probably include one here too for sanity checking
    }

    function totalEquity() public view returns (uint) {
        return totalEquityHeld;
    }

    function totalTrust() public view returns (uint) {
        return totalTrustHeld;
    }

    /**
    * Retrieve the amount of equity tokens owned by the caller
    */

    function personalEquityBalance() public view returns (uint amount) {
        return personalEquity[msg.sender];
    }

    function personalTrustBalance() public view returns (uint amount) {
        return personalTrust[msg.sender];
    }

    function networkTrustBalance(uint networkIndex) public view returns (uint) {
        return networkTrustAllowance[networkIndex][msg.sender];
    }
    
    function networksExisting() public view returns (uint amount) {
        return currentHighestNetworkIndex;
    }

    function _isAdministrator() public view returns (bool) {
        return (administrator == msg.sender);
    }

    function changeAdministrator(address _newAdmin) public isAdministrator {
        require(administrator != msg.sender, "addAdministrator: non-administrators cannot access this.");
        administrator = _newAdmin;
    }

    function _isNetworkMember(address caller, uint networkIndex) private view returns (bool) {
        uint[] memory theirGroups = networkMemberships[caller];
        bool isMember = false;
        for (uint i = 0; i < theirGroups.length; i++) {
            if (theirGroups[i] == networkIndex) {
                isMember = true;
                break;
            }
        }
        return isMember;
    }

    function max(uint a, uint b) private pure returns (uint) {
        return a > b ? a : b;
    }
}
pragma solidity ^0.5.8;

import "../token/interfaces/ERC20_Interface.sol";
import "../utils/SafeMath.sol";

contract Escrow {
    using SafeMath for uint;
    ERC20_Interface daiToken;

    address private bondIssuer;

    constructor (ERC20_Interface _daiInterface, address _bondAddress) public {
        daiToken = _daiInterface;
        bondIssuer = _bondAddress;
    }

    event EscrowInitialised(
        uint bondIndex,
        uint expiryDate,
        uint timestamp
    );

    event EscrowFunded(
        uint bondIndex,
        uint amount,
        uint timestamp
    );

    event EscrowWithdrawn(
        uint bondIndex,
        uint amount,
        uint timestamp
    );

    mapping(uint => uint) private _escrowBalance;
    mapping(uint => uint) private _escrowExpiry;

    function initialise(uint bondIndex, uint expiry) external isBondIssuer {
        require (now < expiry);
        require (_escrowBalance[bondIndex] == 0);
        _escrowExpiry[bondIndex] = expiry;
        emit EscrowInitialised(bondIndex, expiry, now);
    }

    function deposit(uint bondIndex, uint amount) public {
        require(daiToken.transferFrom(msg.sender, address(this), amount));
        _escrowBalance[bondIndex] += amount;
        emit EscrowFunded(bondIndex, amount, now);
    }

    function withdraw(uint bondIndex) external isBondIssuer {
        uint amount = _escrowBalance[bondIndex];
        _escrowBalance[bondIndex] = 0;
        require(daiToken.transfer(msg.sender, amount));
        emit EscrowWithdrawn(bondIndex, amount, now);
    }

    function () external payable {
        revert();
    }
    
    function escrowBalance(uint bondIndex) public view returns (uint) {
        return _escrowBalance[bondIndex];
    }
    
    function escrowExpiry(uint bondIndex) public view returns (uint) {
        return _escrowExpiry[bondIndex];
    }
    
    modifier isBondIssuer() {
        require(msg.sender == bondIssuer);
        _;
    }
}
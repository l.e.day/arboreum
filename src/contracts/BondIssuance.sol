pragma solidity 0.5.8;

import "./token/interfaces/ERC20_Interface.sol";
import "./utils/SafeMath.sol";

contract BondIssuance {
    using SafeMath for uint;
    ERC20_Interface public daiToken;

uint totalHeld;
uint latestBondIndex;
uint internal MAX_INT = 2**256 - 1;
mapping (uint => Bond) bondMapping;
mapping (uint => bool) bondCollateralised;
mapping (uint => uint) bondTokensAvailable;
mapping (uint => address) bondProposer;

address internal _daiAddress;
address internal _trustAddress;
ERC20_Interface internal _daiInterface;

struct Bond {
    uint      trustNetworkIndex;
    address   beneficiary;
    address   guarantor;
    uint      beneficiaryCollateral;
    bool      zerothCollateralPaid;
    uint      guarantorCollateral;
    bool      firstCollateralPaid;
    uint      principal;
    uint      yield;
    uint      totalDue;
    uint      secondLossCollateral;
    bool      secondCollateralPaid;
    uint      expiryTime;
    uint      totalRepaid;
    bool      active;
}

address internal administrator;

constructor (address _dai) public {
    totalHeld = 0;
	latestBondIndex = 0;
	administrator = msg.sender;
	_daiAddress = _dai;
	_daiInterface = ERC20_Interface(_daiAddress);
	daiToken = _daiInterface;
}

uint internal directCoveragePercentage = 50;

uint internal collateralCoverage = 80;

event BondProposed (
    uint    trustNetworkIndex,
    uint    bondIndex,
    address beneficiary,
    address guarantor,
    uint    zerothCollateral,
    uint    firstCollateral,
    uint    principal,
    uint    yield,
    uint    expiry
);

event BondPaymentMade(
    uint    bondIndex,
    address payer,
    uint    amount,
    uint    timestamp
);

event BondFullyCollateralised(
    uint    bondIndex,
    uint    timestamp
);

event BondActivated(
    uint    bondIndex,
    uint    expiry,
    uint    timestamp
);

function viewCurrentDAIContract() public view returns (address) {
	return _daiAddress;
}

function viewCurrentTrustContract() public view returns (address) {
	return _trustAddress;
}

 function setNewDAIContract(address _newDAIAddress) public isAdministrator {
	 daiToken.approve(_trustAddress, 0);
	 _daiAddress = _newDAIAddress;
	 _daiInterface = ERC20_Interface(_daiAddress);
	 daiToken = _daiInterface;
	 daiToken.approve(_trustAddress, MAX_INT);
 }

  function setNewTrustContract(address _newTrustAddress) public isAdministrator {
	 daiToken.approve(_trustAddress, 0);
	 _trustAddress = _newTrustAddress;
	 daiToken.approve(_newTrustAddress, MAX_INT);
 }

function serialiseBondPaymentDetails(uint _bondIndex) external view returns (address, address, uint, bool, uint, bool, bool) {
    Bond memory bond = bondMapping[_bondIndex];
    address          b = bond.beneficiary;
    address          g = bond.guarantor;
    uint          bcol = bond.beneficiaryCollateral;
    bool          bpay = bond.zerothCollateralPaid;
    uint          gcol = bond.guarantorCollateral;
    bool          gpay = bond.firstCollateralPaid;
    bool          spay = bond.secondCollateralPaid;
    return (b, g, bcol, bpay, gcol, gpay, spay);
}

function acknowledgeCollateralDeposit(uint _bondIndex, address _payer) external view {
    require(!bondMapping[_bondIndex].active, "acknowledgeCollateralDeposit: cannot deposit collateral to an active bond.");
    if (_payer == bondMapping[_bondIndex].beneficiary) {
        bondMapping[_bondIndex].zerothCollateralPaid == true;
    }
    else {
        if (_payer == bondMapping[_bondIndex].guarantor) {
            bondMapping[_bondIndex].firstCollateralPaid == true;
        }
        else {
            revert("acknowledgeCollateralDeposit: collateral must be placed by the beneficiary or the guarantor");
        }
    }
}

// Create a bond proposal, giving the details of the bond in question and creating an
// entry in the bondDetails mapping. Returns the index to said bond.
function proposeBond(uint               _trustNetworkIndex,
                     address            _beneficiary,
                     address            _guarantor,
                     uint               _beneficiaryCollateral,
                     uint               _guarantorCollateral,
                     uint               _principal,
                     uint               _yield,
                     uint               _expiry)
                     external returns (uint) {
        uint thisBondIndex = latestBondIndex;
        // The yield must be a number greater than 0 and lower than 100 inclusive (percent).
        require(_yield >= 0 && _yield <= 100, "proposeBond: yield must be between 0 and 100 inclusive");
        uint _directProtection = _beneficiaryCollateral + _guarantorCollateral;
		require((100 * _directProtection) >= (directCoveragePercentage * _principal), "proposeBond: direct coverage is not sufficient.");
        uint _trueTotal = ( _principal * (100 + _yield)) / 100;
        // Now instantiate the bond.
        bondMapping[thisBondIndex] = Bond
             ({ trustNetworkIndex: _trustNetworkIndex,
                beneficiary: _beneficiary,
                guarantor: _guarantor,
                beneficiaryCollateral: _beneficiaryCollateral,
                zerothCollateralPaid: false,
                guarantorCollateral: _guarantorCollateral,
                firstCollateralPaid: false,
                secondCollateralPaid: false,
                principal: _principal,
                yield: _yield,
                totalDue: _trueTotal,
                secondLossCollateral: ((_principal * collateralCoverage) / 100) - _beneficiaryCollateral - _guarantorCollateral,
                // The expiry time will not trigger properly until the bond is activated.
                expiryTime: _expiry,
                totalRepaid: 0,
                active: false
            });
        bondProposer[thisBondIndex] = msg.sender;
        latestBondIndex += 1;
        emit BondProposed(_trustNetworkIndex, thisBondIndex, _beneficiary, _guarantor,
                          _beneficiaryCollateral, _guarantorCollateral, _principal, _yield, _expiry);
        return latestBondIndex;
    }

// TODO: Create a number of getter and setter functions for each component of the bond struct.

// Funds the bond with initial collateral from the beneficiary and trust from the guarantor.
// Note that all funds are stored in the same location so that we can potentially allocate the DAI
// to an interest bearing facility such as compound.finance whilst the bond is active.
// NB: THIS REQUIRES SIGNIFICANT CARE - HUMAN INTERACTION IS A SUBSTANTIAL RISK FACTOR
// Sets the status to Initialised.
function equityFundBond(uint _bondIndex)
                        public onlyTrustContract returns (bool) {
    Bond memory bond = bondMapping[_bondIndex];
    uint amount = bond.secondLossCollateral;
    require(true, "initialiseBond: bond must be in the Proposed state.");
    require(daiToken.allowance(_trustAddress, address(this)) >= amount, "initialiseBond: insufficient allowance.");
    require(daiToken.transferFrom(_trustAddress, address(this), amount), "initialiseBond: could not transfer second loss collateral.");
	totalHeld += amount;
    bondCollateralised[_bondIndex] = true;
    emit BondFullyCollateralised(_bondIndex, now);
    return true;
}

// Activates the bond expiry, and sets the status to Active.
// Interested investors can now purchase up to N ledger tokens (1 token = 1 DAI)
// where N is the principal of the bond.
function activateBond(uint _bondIndex) public isAdministrator returns (bool) {
    Bond storage bond = bondMapping[_bondIndex];
    require(!bond.active, "activateBond: bond must still be inactive.");
    require(bondCollateralised[_bondIndex], "activateBond: bond must be fully collateralised.");
    uint bondLength = bond.expiryTime;
    bond.expiryTime = now + bondLength;
    bond.active = true;
    bondTokensAvailable[_bondIndex] = bond.totalDue;
    emit BondActivated(_bondIndex, bond.expiryTime, now);
    return true;
}

function queryAvailableBondTokens(uint _bondIndex) public view returns (uint) {
    return bondTokensAvailable[_bondIndex];
}

// Mechanism to allow beneficiaries (or any other party) to repay the outstanding balance
// (up to principal * 1.M, where M in (0, 1) is the yield).
function repayBond(uint _bondIndex, uint _amount) external returns (bool) {
    Bond storage bond = bondMapping[_bondIndex];
    require(true, "repayBond: bond must be in the Active state.");
    // Whoever is making a payment must have allowed a certain amount to be transferred.
    require(daiToken.allowance(msg.sender, address(this)) >= _amount, "repayBond: caller has not allowed enough DAI to be transferred.");
    require(daiToken.transferFrom(msg.sender, address(this), _amount), "repayBond: transfer of repayment did not complete.");
    bond.totalRepaid += _amount;
    emit BondPaymentMade(_bondIndex, msg.sender, _amount, now);
    return true;
}

// Once a bond has reached its expiry date, any investor/guarantor/beneficiary of said bond
// can call it, releasing all equity back into the equity pool of the main contract,
function callBond(uint _bondIndex) public view {
    Bond memory bond = bondMapping[_bondIndex];
    require (now > bond.expiryTime, "callBond: bond has not yet reached maturity.");
	uint toRelease = bond.totalRepaid;
	unit fullAmount = bond.totalDue;
	if (toRelease == fullAmount) {
		// release fullAmount back into the equity pool, no other actions to be taken
		totalHeld -= fullAmount;
		daiToken.transferFrom(address(this), _trustAddress, fullAmount);
		emit BondFullyRepaid(_bondIndex, fullAmount, now);
	}
	// Can't see a situation where someone would overpay a bond, but the case needs to be here for completeness.
	else if (toRelease > fullAmount) {
		uint amountOverpaid = toRelease - fullAmount;
		daiToken.transferFrom(address(this), bond.beneficiary, amountOverpaid);
		daiToken.transferFrom(address(this), _trustAddress, fullAmount);
		totalHeld -= toRelease;
		emit BondOverpaid(_bondIndex, fullAmount, toRelease, amountOverpaid, now);
		// resolveOverpayment - function needs to be defined here - pay the excess directly back to the bond recipient, then release the balance to the equity pool
	}
	else {		
		uint amountNotPaid = fullAmount - toRelease;
		totalHeld -= fullAmount;
		emit BondDefaulted(_bondIndex, fullAmount, toRelease, amountNotPaid, now);
		// resolveDefault - function needs to be defined here - is the defaulted amount covered by 0th/1st loss collateral? if so, deduct from appropriate parties and release.
		// this will likely need some direct interaction with the trust network contract in terms of altering equity/trust tokens
	}
}

modifier isAdministrator(){
        _isAdministrator();
        _;
    }

function _isAdministrator() public view returns (bool) {
	return (administrator == msg.sender);
}

modifier onlyTrustContract() {
    require(msg.sender == _trustAddress, "onlyTrustContract: non-trust contract attempting to call administrative function.");
    _;
}

function changeCollateralRate(uint _newCollateralRate) public isAdministrator {
    require(_newCollateralRate >= 50 && _newCollateralRate <= 90, "changeCollateralRate: collateral coverage percentage must be between 50-90%");
    collateralCoverage = _newCollateralRate;
}

function changeDirectCoverageNumerator(uint _newPercent) public isAdministrator {
	require (((0 <= _newPercent) && (_newPercent <= 100)), "changeDirectCoverageNumerator: cannot have direct coverage of over 100%.");
    directCoveragePercentage = _newPercent;
}


function designateTrustNetworkContract(address _newTrustContract) public isAdministrator {
    require(_newTrustContract != address(0), "designateTrustNetworkContract: cannot designate to zero address");
    _trustAddress = _newTrustContract;
}

}
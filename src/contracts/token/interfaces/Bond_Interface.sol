pragma solidity ^0.5.8;

interface Bond_Interface {

    function serialiseBondPaymentDetails(uint) external view returns (address, address, uint, bool, uint, bool, bool);

    function acknowledgeCollateralDeposit(uint, address) external;

    function proposeBond(uint, address, address, uint, uint, uint, uint, uint) external returns (uint);

    function equityFundBond(uint) external returns (bool);

    function activateBond(uint) external;

    function repayBond(uint, uint) external;

    function isOperator() external view returns (bool);

}